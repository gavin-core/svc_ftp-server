process.on('uncaughtException', function () {
  console.log('UNCAUGHT EXCEPTION'.red)
  console.log(JSON.stringify(arguments))
})

const core = require('node-core')
const modules = require('./modules')
const serverLogger = modules.logger.ServerLogger
const FTPServer = modules.FTPServer

core.pathUtils.ensureExists(core.config.filesystem.root, () => {
  const server = new FTPServer({
    port: core.config.server.port,
    host: core.config.server.host,
    root: core.config.filesystem.root,
    allowAnonLogin: core.config.server.allowAnonLogin
  })

  server.on('listening', () => {
    serverLogger.verbose(`FTP Server started on ${server.address.address}:${server.address.port}`)
  })
})
