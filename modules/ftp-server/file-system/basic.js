const fs = require('fs')
const path = require('path')
const Base = require('./base')

const Basic = function (root, opts) {
  const _this = this

  _this.fullCwd = () => {
    return path.resolve(_this.root + _this.abstractCwd)
  }

  _this.getFiles = cb => {
    fs.readdir(_this.fullCwd(), cb)
  }

  _this.getFileList = cb => {
    _this.getFiles((err, files) => {
      if (err) {
        return cb(err)
      }

      if (!files || !files.length) {
        return cb(null, [])
      }

      const result = []

      for (let i = 0; i < files.length; i++) {
        Base.getFileStatDetailed(path.join(_this.fullCwd(), files[i]), files[i], (err, fileDetail) => {
          if (err) {
            return cb(err)
          }

          result.push(fileDetail)

          if (result.length === files.length) {
            cb(null, result)
          }
        })
      }
    })
  }

  _this.getFileStat = (filename, cb) => {
    fs.stat(_this.fullCwd() + path.sep + filename, cb)
  }

  _this.chdir = (dir, cb) => {
    if (dir.match(/^\//)) {
      _this.abstractCwd = dir
    } else if (_this.abstractCwd.match(/^\/$/)) {
      _this.abstractCwd += dir
    } else {
      _this.abstractCwd += path.sep + dir
    }

    _this.abstractCwd = path.resolve(_this.abstractCwd)

    fs.stat(_this.fullCwd(), err => {
      if (err) {
        return cb(new Error('No such directory'))
      }
      cb()
    })
  }

  _this.mkdir = (directory, cb) => {
    Base.mkdir(path.join(_this.fullCwd(), directory), cb)
  }

  _this.rmdir = (directory, cb) => {
    Base.rmdir(path.join(_this.fullCwd(), directory), cb)
  }

  _this.readFile = (filename, opts, cb) => {
    Base.createReadStream(path.join(_this.fullCwd(), filename), opts, cb)
  }
  _this.writeFile = (filename, opts, cb) => {
    Base.createWriteStream(path.join(_this.fullCwd(), filename), opts, cb)
  }
  _this.rename = (from, to, cb) => {
    Base.rename(path.join(_this.fullCwd(), from), path.join(_this.fullCwd(), to), cb)
  }
  _this.deleteFile = (filename, cb) => {
    Base.deleteFile(path.join(_this.fullCwd(), filename), cb)
  }

  return _this
}

module.exports = Basic
