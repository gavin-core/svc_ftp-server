require('./date-format')

const core = require('node-core')
const fs = require('fs')

module.exports = {
	getFiles: (filepath, cb) => {
		fs.readdir(filepath, cb);
	},
	getFilePermissions: function () {
		let cb
		let filepath
		let fileStat

		switch (arguments.length) {
			case 2:
				if (typeof arguments[0] === 'string') {
					filepath = arguments[0];
				}
				else {
					fileStat = arguments[0];
				}
				cb = arguments[1];
				break
			default:
				throw new Error('ERROR')
		}

		if (!fileStat){
			return module.exports
				.getFileStat(filepath, (err, fileStat) => {
					module.exports.getFilePermissions(fileStat, cb)
				})
		}

		let result = ''
		result += fileStat.isDirectory() ? 'd' : '-'
		result += (0400 & fileStat.mode) ? 'r' : '-'
		result += (0200 & fileStat.mode) ? 'w' : '-'
		result += (0100 & fileStat.mode) ? 'x' : '-'
		result += (040 & fileStat.mode) ? 'r' : '-'
		result += (020 & fileStat.mode) ? 'w' : '-'
		result += (010 & fileStat.mode) ? 'x' : '-'
		result += (04 & fileStat.mode) ? 'r' : '-'
		result += (02 & fileStat.mode) ? 'w' : '-'
		result += (01 & fileStat.mode) ? 'x' : '-'
		
		cb(null, result)
	},
	getFileStat: (filepath, cb) => {
		fs.stat(filepath, cb)
	},
	getFileStatDetailed: (filepath, alias, cb) => {
		module.exports.getFileStat(filepath, (err, fileStat) => {
			if (err) {
				return cb(err)
			}

			module.exports.getFilePermissions(fileStat, (err, permstr) => {
				if (err) {
					return cb(err)
				}

				let result = ''
				result += permstr
				result += ' 4 ftp  '
				result += core.strings.leftPad(fileStat.size.toString(), 12) + ' '
				result += core.strings.leftPad(new Date(fileStat.mtime).format('M d H:i'), 12) + ' ' // need to use a date string formatting lib
				result += alias

				cb(null, result)
			})
		})
	},
	createReadStream: (filepath, opts, cb) => {
		module.exports.getFileStat(filepath, (err, stat) => {
			if (err) {
				return cb(err)
			}

			if (!stat.isFile()) {
				return cb("No such file")
			}

			cb(null, fs.createReadStream(filepath, opts || {}));
		})
	},
	createWriteStream: (filepath, opts, cb) => {
		opts = core._.extend({ mode: 0666, flags: 'w' }, opts);
		cb(null, fs.createWriteStream(filepath, opts));
	},
	rename: fs.rename,
	deleteFile: (filepath, cb) => {
		module.exports.getFileStat(filepath, (err, stat) => {
			if (err) {
				return cb(err)
			}

			if (!stat) {
				return cb('No such file')
			}

			if (stat.isDirectory()) {
				return cb('Invalid command to delete a folder')
			}

			fs.unlink(filepath, cb)
		})
	},
	mkdir: fs.mkdir,
	rmdir: fs.rmdir
}