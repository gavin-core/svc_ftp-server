const Basic = require('./basic')
const UserDefined = require('./user-defined')

/*
const fake = [
  {
    directory: '/home/gavin/Projects/ftp-folder-root/First/Test',
    alias: '/',
    primary: true,
    actions: {
      read: true,
      write: true,
      append: true,
      remove: true
    },
    directories: {
      create: true,
      remove: true,
      list: true,
      recursive: true
    }
  },
  {
    directory: '/home/gavin/Projects/ftp-folder-root/Second/Test',
    alias: 'Whap',
    actions: {
      read: true,
      write: false,
      append: true,
      remove: true
    },
    directories: {
      create: true,
      remove: true,
      list: true,
      recursive: true
    }
  },
  {
    directory: '/home/gavin/Projects/ftp-folder-root/Third',
    alias: 'Whoop',
    actions: {
      read: false,
      write: true,
      append: true,
      remove: true
    },
    directories: {
      create: true,
      remove: true,
      list: true,
      recursive: false
    }
  }
]
*/

const FileSystem = function (root, userDirectories) {
  const _this = this
  const _userDirectories = userDirectories// = fake; //TODO - remove fake -- testing purposes

  _this.root = root
  _this.abstractCwd = '/'

  if (_userDirectories && _userDirectories.length) {
    UserDefined.call(_this, _userDirectories)
  } else {
    Basic.call(_this)
  }
}

module.exports = FileSystem
