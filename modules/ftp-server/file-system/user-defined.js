const fs = require('fs')
const path = require('path')
const Base = require('./base')

const UserDefined = function (userDirectories) {
  const _this = this
  const _userDirectories = userDirectories
  let _primaryDirectory

  ;(function prepareUserDirectories () {
    for (let i = 0; i < _userDirectories.length; i++) {
      let userDirObj = _userDirectories[i]

      if (userDirObj.directory.lastIndexOf(path.sep) === userDirObj.directory.length) {
        userDirObj.directory = userDirObj.directory.substr(0, userDirObj.directory.length - 1)
      }

      if (userDirObj.primary) {
        userDirObj.alias = '/'
        _primaryDirectory = userDirObj
        _this.root = userDirObj.directory
        continue
      }
    }
  })()

  const _validateFile = (filename, cb) => {
    _this.getFileStat(filename, (err, stat) => {
      if (err) {
        return cb(err, filename)
      }

      if (stat.isDirectory()) {
        return cb(null, filename, false)
      }

      cb(err, filename, true)
    })
  }

  _this.cwdIsRoot = () => _this.abstractCwd.match(/^\/$/)

  _this.fullCwd = () => {
    if (_this.cwdIsRoot()) {
      return _this.root
    }

    const parts = _this.abstractCwd.split('/')
    parts.splice(0, _this.cwdSettings.primary ? 1 : 2, '')

    return _this.cwdSettings.directory + parts.join(path.sep)
  }

  _this.fullFilename = filename => _this.fullCwd() + path.sep + filename

  _this.getRootDir = (abstracted, cb) => {
    if (!_userDirectories.length) {
      return _this.getFiles(cb)
    }

    let result = []

    for (let i = 0; i < _userDirectories.length; i++) {
      if (!_userDirectories[i].primary) {
        result.push(abstracted ? _userDirectories[i].alias : _userDirectories[i].directory)
      }
    }

    Base.getFiles(_this.root, (err, files) => {
      if (err) {
        return cb(err)
      }

      if (_primaryDirectory.directories.recursive) {
        result = result.concat(files)
        return cb(null, result)
      }

      const deniedFiles = []
      const authedFiles = []

      for (let i = 0; i < files.length; i++) {
        _validateFile(files[i], (err, filename, valid) => {
          if (!err && valid) {
            authedFiles.push(filename)
          } else {
            deniedFiles.push(filename)
          }

          if (deniedFiles.concat(authedFiles).length === files.length) {
            return cb(null, result.concat(authedFiles))
          }
        })
      }
    })
  }

  _this.getFiles = (abstracted, cb) => {
    if (_this.cwdIsRoot()) {
      return this.getRootDir(abstracted, cb)
    }

    if (!_this.cwdSettings.directories.list) {
      return cb(null, [])
    }

    return Base.getFiles(_this.fullCwd(), (err, files) => {
      if (err) {
        return cb(err)
      }

      if (!files.length || _this.cwdSettings.directories.recursive) {
        return cb(null, files)
      }

      const deniedFiles = []
      const authedFiles = []

      for (let i = 0; i < files.length; i++) {
        _validateFile(files[i], (err, filename, valid) => {
          if (!err && valid) {
            authedFiles.push(filename)
          } else {
            deniedFiles.push(filename)
          }

          if (deniedFiles.concat(authedFiles).length === files.length) {
            return cb(null, authedFiles)
          }
        })
      }
    })
  }

  _this.getFileList = cb => {
    const abstractFileDetail = (filename, cb) => {
      let alias = filename

      if (filename.indexOf(path.sep) < 0) {
        filename = path.join(_this.fullCwd(), filename)
      }

      for (let i = 0; i < _userDirectories.length; i++) {
        if (_userDirectories[i].directory === filename) {
          alias = _userDirectories[i].alias
          break
        }
      }

      Base.getFileStatDetailed(filename, alias, cb)
    }

    _this.getFiles(false, (err, files) => {
      if (err) {
        return cb(err)
      }

      if (!files || !files.length) {
        return cb(null, [])
      }

      const getFileDetail = (files, cb, result = []) => {
        if (!files || !files.length) {
          return cb(null, result)
        }

        const file = files.shift()

        abstractFileDetail(file, (err, fileDetail) => {
          if (err) {
            return getFileDetail(files, cb, result)
          }

          result.push(fileDetail)
          return getFileDetail(files, cb, result)
        })
      }

      getFileDetail(files, cb)
    })
  }

  _this.getFileStat = (filename, cb) => {
    if (_this.cwdIsRoot()) {
      for (let i = 0; i < _userDirectories.length; i++) {
        if (_userDirectories[i].alias === filename) {
          return fs.stat(_userDirectories[i].directory, cb)
        }
      }
    }

    fs.stat(_this.fullCwd() + path.sep + filename, cb)
  }

  _this.chdir = (dir, cb) => {
    cb = cb || (() => {})

    if (dir.match(/^\//)) {
      _this.abstractCwd = dir
    } else if (_this.abstractCwd.match(/^\/$/)) {
      _this.abstractCwd += dir
    } else {
      _this.abstractCwd += path.sep + dir
    }

    _this.abstractCwd = path.resolve(_this.abstractCwd)
      .replace(/^[A-Za-z]:/, '')
      .replace(/\\/g, '/')

    _this.maintainCwd(() => {
      fs.stat(_this.fullCwd(), err => {
        if (err) {
          return cb(new Error('No such directory'))
        }

        cb()
      })
    })
  }

  _this.mkdir = (directory, cb) => {
    if (!_this.cwdSettings.directories.create) {
      return cb(new Error(`No permissions to create directory: ${directory}`))
    }

    if (_this.cwdIsRoot()) {
      for (let i = 0; i < _userDirectories.length; i++) {
        if (directory === _userDirectories[i].alias) {
          return cb(new Error(`${directory} already exists`))
        }
      }
    }

    Base.mkdir(path.join(_this.fullCwd(), directory), cb)
  }

  _this.rmdir = (directory, cb) => {
    if (!_this.cwdSettings.directories.remove) {
      return cb(new Error(`No permissions to remove directory: ${directory}`))
    }

    if (_this.cwdIsRoot()) {
      for (let i = 0; i < _userDirectories.length; i++) {
        if (directory === _userDirectories[i].alias) {
          return cb(new Error(`No permissions to remove directory: ${directory}`))
        }
      }
    }

    Base.rmdir(path.join(_this.fullCwd(), directory), cb)
  }

  _this.readFile = (filename, opts, cb) => {
    if (!_this.cwdSettings.actions.read) {
      return cb(new Error(`No permissions to read file: ${filename}`))
    }

    Base.createReadStream(path.join(_this.fullCwd(), filename), opts, cb)
  }

  _this.writeFile = (filename, opts, cb) => {
    if (!_this.cwdSettings.actions.write) {
      return cb(new Error(`No permissions to write to directory: ${_this.abstractCwd}`))
    }

    Base.createWriteStream(path.join(_this.fullCwd(), filename), opts, cb)
  }

  _this.rename = (from, to, cb) => {
    if (!_this.cwdSettings.actions.rename) {
      return cb(new Error(`No permissions to rename file: ${from}`))
    }

    if (_this.cwdIsRoot()) {
      for (let i = 0; i < _userDirectories.length; i++) {
        if (from === _userDirectories[i].alias) {
          return cb(new Error('Cannot rename this directory'))
        } else if (to === _userDirectories[i].alias) {
          return cb(new Error('Entity with matching name already exists'))
        }
      }
    }

    Base.rename(path.join(_this.fullCwd(), from), path.join(_this.fullCwd(), to), cb)
  }

  _this.deleteFile = (filename, cb, force) => {
    if (!force && !_this.cwdSettings.actions.remove) {
      return cb(new Error(`No permissions to delete file: ${filename}`))
    }

    Base.deleteFile(path.join(_this.fullCwd(), filename), cb)
  }

  _this.maintainCwd = cb => {
    cb = cb || (() => {})

    if (!_this.cwdIsRoot()) {
      const topDir = _this.abstractCwd.split('/')[1]

      for (let i = 0; i < _userDirectories.length; i++) {
        if (_userDirectories[i].alias === topDir) {
          _this.cwdSettings = _userDirectories[i]
          return cb()
        }
      }
    }

    for (let i = 0; i < _userDirectories.length; i++) {
      if (_userDirectories[i].primary) {
        _this.cwdSettings = _userDirectories[i]
        return cb()
      }
    }
  }

  _this.chdir('/')
  return _this
}

module.exports = UserDefined
