const core = require('node-core')
const discovery = require('node-discovery')
const network = require('node-network')
const events = require('events')
const net = require('net')
const db = require('../../middleware/db')
const logger = require('../logger')
const FileSystem = require('./file-system')

const statusMessages = {
  // 100 SERIES =
  '110': 'Restart marker reply.', // In this case, the text is exact and not left to the particular implementation; it must read: MARK yyyy = mmmm Where yyyy is User-process data stream marker, and mmmm server's equivalent marker (note the spaces between markers and "=").
  '120': 'Service ready in %s sec.',
  '125': 'Data connection already open; transfer starting.',
  '150': 'File status okay; about to open data connection.',

  // 200 SERIES =
  '200': 'Command okay.',
  '202': 'Command not implemented, superfluous at this site.',
  '211': 'System status, or system help reply.',
  '212': 'Directory status.',
  '213': 'File status.',
  '214': 'Help message.', // On how to use the server or the meaning of a particular non-standard command. This reply is useful only to the human user.
  '215': 'NodeFTP server emulator.', // NAME system type. Where NAME is an official system name from the list in the Assigned Numbers document.
  '220': 'Service ready for new user.',
  '221': 'Service closing control connection.', // Logged out if appropriate.
  '225': 'Data connection open; no transfer in progress.',
  '226': 'Closing data connection.', // Requested file action successful (for example, file transfer or file abort).
  '227': 'Entering Passive Mode.', // (h1,h2,h3,h4,p1,p2).
  '230': `Authenticated, Welcome to ${discovery.servicePackage._name}`,
  '250': 'Requested file action okay, completed.',
  '257': '"%s" created.',

  // 300 SERIES =
  '331': 'User name okay, need password.',
  '332': 'Need account for login.',
  '350': 'Requested file action pending further information.',

  // 400 SERIES =
  '421': 'Service not available, closing control connection.', // This may be a reply to any command if the service knows it must shut down.
  '425': "Can't open data connection.",
  '426': 'Connection closed; transfer aborted.',
  '450': 'Requested file action not taken.', // File unavailable (e.g., file busy).
  '451': 'Requested action aborted. Local error in processing.',
  '452': 'Requested action not taken.', // Insufficient storage space in system.

  // 500 SERIES =
  '500': 'Syntax error, command unrecognized.', // This may include errors such as command line too long.
  '501': 'Syntax error in parameters or arguments.',
  '502': 'Command not implemented.',
  '503': 'Bad sequence of commands.',
  '504': 'Command not implemented for that parameter.',
  '530': 'Not logged in.',
  '532': 'Need account for storing files.',
  '550': 'Requested file action not taken.', // File unavailable (e.g., file not found, no access).
  '551': 'Requested action aborted. Page type unknown.',
  '552': 'Requested file action aborted.', // Exceeded storage allocation (for current directory or dataset).
  '553': 'Requested action not taken.' // File name not allowed.
}

const FTPCommandSocket = function (server, socket, opts) {
  const _this = this
  const _server = server
  const _socket = socket
  const _opts = opts
  const _logger = new logger.SocketLogger(_socket.remoteAddress, _socket.remotePort)

  let _busy = false
  let _queue = []

  const _joinArguments = (args, sep) => {
    if (!sep) {
      sep = ' '
    }

    if (Array.isArray(args)) {
      return args.join(sep)
    }

    let result = ''
    for (let item in args) {
      if (args.hasOwnProperty(item)) {
        result += (+item ? sep : '') + args[item]
      }
    }

    return result.replace(/'/g, '')
  }

  const _customCommands = {
    error: (command, err) => {
      if (err && typeof err !== 'string') {
        err = JSON.stringify(err)
      }

      if (err && command) {
        return _this.reply(502, `${command} ${err}`)
      } else if (err) {
        return _this.reply(502, err)
      }

      _this.reply(502, command)
    }
  }

  const _authlessCommands = {
    PASS: function onPasswordProvided (password) {
      const initialize = directories => {
        _this.fs = new FileSystem(_opts.root, directories)
        _logger.onUserAuthenticated(_this.user)
        _this.reply(230)
      }

      const isAllowedUsername = _this.username === 'anon' || _this.username === 'anonymous'

      if (_opts.allowAnonLogin && (isAllowedUsername)) {
        return initialize()
      }

      db.getDb().users.findOne({ username: _this.username }, (err, user) => {
        if (err || !user) {
          return _this.reply(530, `${_this.username} is not permitted to connect`)
        }

        if (!user.enabled) {
          return _this.reply(530, 'This account has been deactivated. Please speak to the administrators to resolve')
        }

        if (user.password !== password) {
          return _this.reply(530, 'Password incorrect')
        }

        _this.user = user

        db.getDb().user_directories.find({ userId: _this.user._id }, (err, directories) => {
          if (err) {
            return _commandHandlers['error'](err)
          }

          initialize(directories)
        })
      })
    },
    QUIT: function onDisconnect () {
      _this.reply(221, 'Goodbye\r\n')
      _socket.end()
    },
    TYPE: function setEncoding (dataEncoding) {
      switch (dataEncoding) {
        case 'A':
          _this.dataEncoding = 'ascii'
          return _this.reply(200)
        case 'I':
          _this.dataEncoding = 'binary'
          return _this.reply(200)
        default:
          return _this.reply(501)
      }
    },
    USER: function onUsernameProvided (username) {
      _this.username = username || 'anonymous'
      _this.reply(331, `Password required for ${username}`)
    }
  }

  const _authRequiredCommand = {
    CDUP: function moveBackOneDirectory () {
      _authRequiredCommand.CWD.call(this, '..')
    },
    CWD: function changeWorkingDirectory () {
      const directory = _joinArguments(arguments)

      _this.fs.chdir(directory, err => {
        if (err) {
          return _this.reply(431, err)
        }

        _this.reply(200)
      })
    },
    DELE: function onFileDeleteRequest () {
      const filename = _joinArguments(arguments)

      _this.fs.deleteFile(filename, (err, result) => {
        if (err) {
          return _this.reply(550, err)
        }

        _this.reply(250)
      })
    },
    MDTM: function onFileLastModifiedTimeRequest () {
      const filename = _joinArguments(arguments)

      _this.fs.getFileStat(filename, (err, stat) => {
        if (err) {
          return _this.reply(550, err)
        }

        _this.reply(200, stat.mtime)
      })
    },
    MKD: function onDirectoryCreateRequest () {
      const directory = _joinArguments(arguments)

      _this.fs.mkdir(directory, err => {
        if (err) {
          return _this.reply(553, err)
        }

        _this.reply(200)
      })
    },
    PWD: function printWorkingDirectory () {
      _this.reply(200, `'${_this.fs.abstractCwd}'`)
    },
    PASV: function enterPassiveMode () {
      network.portUtils.getPort(core.config.server.data.portRange, (err, port) => {
        // WE SHOULDN'T GET AN ERROR
        if (err) {
          this._logger.log(err)
        }

        const dataUriObj = new network.uriUtils.URI('tcp', _server.address.address, port)
        const dataServer = net.createServer()
        let dataSocket
        let timeout

        dataServer.on('connection', _dataSocket => {
          clearTimeout(timeout)
          dataServer.close()

          dataSocket = _dataSocket
          dataSocket.setTimeout(5000)
        })

        dataServer.on('error', err => {
          logger.ServerLogger.error(`DATA SERVER ERROR: ${JSON.stringify(err)}`)
          _this.reply(500, 'Internal server error')
        })

        dataServer.listen(dataUriObj.port, dataUriObj.address, function onListening () {
          const onAcceptConnection = cb => {
            let interval
            const continueCB = () => {
              _this.reply(150)
              if (interval) {
                clearInterval(interval)
              }

              cb(dataSocket)
            }

            if (dataSocket) {
              return continueCB()
            }

            interval = setInterval(() => {
              if (!dataSocket) {
                return
              }

              clearInterval(interval)
              continueCB()
            }, 100)
          }

          const onComplete = (err, code) => {
            if (dataSocket) {
              dataSocket.end()
            }

            if (err) {
              logger.ServerLogger.error(err)

              if (!code) {
                return _customCommands['error'](null, err)
              }

              return _this.reply(code, err)
            }

            _this.reply(200)
          }

          _queue.unshift((command, args) => {
            _dependantCommands[command](onAcceptConnection, onComplete, args)
          })

          _this.reply(227, `Entering Passive Mode (${dataUriObj.to256()})`)
          timeout = setTimeout(dataServer.close, 10000)
        })
      })
    },
    PORT: function enterActiveMode (address256) {
      if (!address256) {
        return _this.reply(501)
      }

      const uri = new network.uriUtils.URI('ftp', address256)
      const _dataSocket = net.createConnection(uri.port, uri.host)

      _dataSocket.on('error', () => {
        console.log('PASSIVE SOCKET ERROR: ' + JSON.stringify(arguments))
        _this.reply(425)
      })

      _dataSocket.on('connect', function () {
        const onAcceptConnection = cb => {
          _this.reply(150)
          cb(_dataSocket)
        }

        const onComplete = (err, code) => {
          if (_dataSocket) {
            _dataSocket.end()
          }

          if (err) {
            logger.ServerLogger.error(err)

            if (!code) {
              return _customCommands['error'](null, err)
            }

            return _this.reply(code, err)
          }

          _this.reply(200)
        }

        _queue.unshift((command, args) => {
          _dependantCommands[command](onAcceptConnection, onComplete, args)
        })

        _this.reply(200)
      })
    },
    RMD: function onDirectoryRemoveRequest () {
      const directory = _joinArguments(arguments)

      _this.fs.rmdir(directory, (err, result) => {
        if (err) {
          return _this.reply(553)
        }

        _this.reply(200)
      })
    },
    RNFR: function onRenameFromRequest () {
      const filename = _joinArguments(arguments)

      _queue.unshift((command, args) => {
        _dependantCommands[command](filename, args)
      })

      _this.reply(200)
    },
    SIZE: function getFileSize () {
      const filename = _joinArguments(arguments)

      _this.fs.getFileStat(filename, (err, stat) => {
        if (err) {
          return _this.reply(450, `Error getting size for file: ${filename}`)
        }

        _this.reply(200, stat.size)
      })
    }
  }

  const _dependantCommands = {
    APPE: function onAppendFileRequest (acceptConnectionCB, completeCB, args) {
      const filename = _joinArguments(args)

      _this.fs.getFileStat(filename, (err, stat) => {
        if (err) {
          return _this.reply(450, `Error getting size for file: ${filename}`)
        }

        _dependantCommands.STOR(acceptConnectionCB, completeCB, args, { start: stat.size, flags: 'r+' })
      })
    },
    LIST: function onListRequest (acceptConnectionCB, completeCB) {
      _this.fs.getFileList((err, files) => {
        if (err) {
          return completeCB(err)
        }

        acceptConnectionCB(dataSocket => {
          if (!dataSocket) {
            return completeCB(`Command must be preceded by a 'PASV' or 'PORT' command`)
          }

          for (let i = 0; i < files.length; i++) {
            dataSocket.write(`${files[i]}\r\n`)
          }
          dataSocket.write('\r\n')

          completeCB()
        })
      })
    },
    REST: function onRequestToResumeUploadOrDownload (acceptConnectionCB, completeCB, args) {
      const opts = {
        start: +args[0]
      }

      _queue.unshift((command, args) => {
        _dependantCommands[command](acceptConnectionCB, completeCB, args, opts)
      })

      _this.reply(200)
    },
    RETR: function onFileRetrieveRequest (acceptConnectionCB, completeCB, args, opts) {
      const filename = _joinArguments(args)

      _this.fs.readFile(filename, opts, (err, stream) => {
        if (err) {
          return completeCB(err)
        }

        acceptConnectionCB(dataSocket => {
          if (!dataSocket) {
            return completeCB(`Command must be preceded by a 'PASV' or 'PORT' command`)
          }

          dataSocket.on('error', err => completeCB(err.toString()))

          stream.on('end', () => completeCB())
          stream.on('error', () => completeCB(err.toString()))
          stream.pipe(dataSocket)
        })
      })
    },
    RNTO: function onRenameToRequest (oldName, args) {
      const newName = _joinArguments(args)

      _this.fs.rename(oldName, newName, err => {
        if (err) {
          return _this.reply(553, err)
        }

        _this.reply(200)
      })
    },
    STOR: function onFileStoreRequest (acceptConnectionCB, completeCB, args, opts) {
      const filename = _joinArguments(args)

      _this.fs.writeFile(filename, opts, (err, stream) => {
        if (err) {
          return completeCB(err, 550)
        }

        acceptConnectionCB(dataSocket => {
          if (!dataSocket) {
            return completeCB(`Command must be preceded by a 'PASV' or 'PORT' command`)
          }

          dataSocket.on('end', () => completeCB())
          dataSocket.pipe(stream)
        })
      })
    }
  }

  _socket.setTimeout(120000) // 2mins - we don't want to rebuild fs too often
  _socket.on('data', chunk => {
    const msg = chunk.toString().trim()
    const parts = msg.split(' ')
    const command = parts[0].trim().toUpperCase()
    const args = parts.slice(1, parts.length)

    _logger.onCommand(command, args.join(' '))

    if (_busy) {
      return _this.write(120)
    }

    _this.busy = true
    _this.setReplyContext(command)

    if (command in _authlessCommands) {
      return _authlessCommands[command].apply(_authlessCommands[command], args)
    }

    if (!_this.fs) {
      return _this.reply(530)
    }

    if (command in _authRequiredCommand) {
      return _authRequiredCommand[command].apply(_authRequiredCommand[command], args)
    }

    if (command in _dependantCommands) {
      if (+_queue.length) {
        return _queue.shift()(command, args)
      }

      return _this.reply(503, 'Command must be preceded by a another command')
    }

    return _this.reply(502)
  })

  _socket.on('end', () => _logger.onDisconnect())

  _socket.on('error', function () {
    _logger.onError.apply(_logger.onError, arguments)
  })

  _this.user = null
  _this.dataEncoding = 'binary'
  _socket.setEncoding(_this.dataEncoding)

  _this.setReplyContext = command => {
    _this.reply = (status, message, cb) => {
      if (!message) {
        message = statusMessages[status] || 'No information'
      }

      _logger.onResponse(command, status, message)
      _socket.write(`${status} ${message}\r\n`, () => {
        _busy = false
        ;(cb || (() => {}))()
      })
    }
  }

  _this.setReplyContext()
  _this.reply(220)

  return _this
}

FTPCommandSocket.prototype.__proto__ = events.EventEmitter.prototype
module.exports = FTPCommandSocket
