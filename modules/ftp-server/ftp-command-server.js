const network = require('node-network')
const events = require('events')
const net = require('net')
const logger = require('../logger').ServerLogger
const FTPCommandSocket = require('./ftp-command-socket')

const FTPCommandServer = function (opts) {
  if (!opts.root) {
    throw new Error('No root directory specified in the passed options')
  }

  const _this = this
  const _serverOpts = new network.uriUtils.URI('ftp', opts.host || '0.0.0.0', opts.port || 21)
  const _server = net.createServer(_serverOpts)

  _server.on('connection', socket => {
    socket = new FTPCommandSocket(_this, socket, { root: opts.root, allowAnonLogin: opts.allowAnonLogin })
  })

  _server.on('error', function () {
    logger.error(`command server error = ${JSON.stringify(arguments)}`)
  })

  _server.listen(_serverOpts.port, _serverOpts.host, () => {
    _this.address = _server.address()
    _this.emit('listening')
    logger.verbose(`Command server started on ${new Date().toString()}`)
  })

  return _this
}

FTPCommandServer.prototype.__proto__ = events.EventEmitter.prototype
module.exports = FTPCommandServer
