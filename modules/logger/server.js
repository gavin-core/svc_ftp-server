const db = require('../../middleware/db').getLogDb()

module.exports = {
  error: message => {
    module.exports.log(1, message, 'red')
  },
  warning: message => {
    module.exports.log(2, message, 'yellow')
  },
  verbose: message => {
    module.exports.log(3, message, 'white')
  },
  debug: message => {
    module.exports.log(4, message, 'grey')
  },
  log: (type, message, color) => {
    if (typeof message !== 'string') {
      message = JSON.stringify(message)
    }

    console.log(message.replace('\r\n', '')[color || 'white'])

    db.servers.save({
      type: type,
      message: message,
      date: new Date()
    })
  }
}
