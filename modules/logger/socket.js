const db = require('../../middleware/db').getLogDb()

const SocketLogger = function (remoteAddress, remotePort) {
  const _this = this
  const _connInfo = {
    remoteAddress: remoteAddress,
    remotePort: remotePort
  }

  let _username
  let _userId

  _this.onCommand = (cmd, params) => {
    _this.log(`COMMAND: ${cmd} ${params}`, 'blue')

    db.socket_commands.insert({
      cmd: cmd,
      params: params,
      userId: _userId,
      username: _username,
      connInfo: _connInfo
    })
  }

  _this.onResponse = (cmd, status, message) => {
    _this.log(`RESPONSE: ${status} ${message}`, 'magenta')

    db.socket_responses.insert({
      cmd: cmd,
      status: status,
      message: message,
      userId: _userId,
      username: _username,
      connInfo: _connInfo
    })
  }

  _this.onError = function () {
    _this.log(arguments, 'red')

    db.socket_errors.insert({
      userId: _userId,
      username: _username,
      error: arguments,
      connInfo: _connInfo
    })
  }

  _this.onDisconnect = () => {
    _this.log('Data socket closed', 'grey')

    db.sockets.insert({
      type: 'disconnect',
      date: new Date(),
      connInfo: _connInfo
    })
  }

  _this.onUserAuthenticated = user => {
    if (typeof user === 'string') {
      _username = user
      return
    }

    _username = (user && user.username) || 'anonymous'
    _userId = user && user._id

    db.user_auths.insert({
      userId: _userId,
      username: _username,
      date: new Date(),
      connInfo: _connInfo
    })
  }

  _this.log = (message, color) => {
    if (typeof message !== 'string') {
      message = JSON.stringify(message)
    }

    console.log(message.replace('\r\n', '')[color || 'white'])
  }

  ;(function onConnect () {
    _this.log('Command client connected', 'grey')

    db.sockets.insert({
      type: 'connect',
      date: new Date(),
      connInfo: _connInfo
    })
  })()

  return _this
}

module.exports = SocketLogger
