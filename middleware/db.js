const core = require('node-core')
const discovery = require('node-discovery')
const mongo = discovery.mongojs

if (!core.config.dbUri) {
  throw new Error('No backend database specified in the config file of the ftp server... A conn string is required for the server to work')
}

if (!core.config.logDbUri) {
  throw new Error('No log database specified in the config file of the ftp server... A conn string is required for the server to work')
}

const db = mongo(core.config.dbUri, [
  'users',
  'user_directories',
  'logs'
])

const logDb = mongo(core.config.logDbUri, [
  'sockets',
  'socket_commands',
  'socket_responses',
  'socket_errors',
  'servers',
  'user_auths'
])

module.exports = {
  getDb: () => db,
  getLogDb: () => logDb
}
