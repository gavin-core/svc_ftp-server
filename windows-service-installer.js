const discovery = require('node-discovery')
const process = require('process')
const nodeWindows = require('node-windows')
const Service = nodeWindows.Service

if (process.platform !== 'win32') {
  throw new Error('This script is intended for Windows based operating systems. Terminating')
}

// Create a new service object
const svc = new Service({
  name: discovery.servicePackage.name,
  description: discovery.servicePackage.description,
  script: '.\\index.js'
})

// Listen for the "install" event, which indicates the
// process is available as a service.
svc.on('install', () => svc.start())

svc.on('uninstall', () => {
  console.log('Uninstall complete.')
  console.log(`The service exists: ${svc.exists}`)
})

if (process.argv[2] === '--install') {
  svc.install()
} else if (process.argv[2] === '--uninstall') {
  svc.uninstall()
} else {
  throw new Error('Accepted commands are: node windows-service-installer.js --install/--uninstall')
}
